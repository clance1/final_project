#!/usr/bin/env python3

import json

f = open('tornadoes.json')
tornadoes = json.load(f)

for tornado in tornadoes:
    print(json.dumps(tornado) + ",")
