#!/usr/bin/env python3

# IMPORTS

import cherrypy
from _natural_disasters_database import _natural_disasters_database
from landslide_controller import LandslideController

# FUNCTIONS

def start_service():

    # Database loading

    nddb = _natural_disasters_database() # should load automatically

    # Controller Specifications

    lcon = LandslideController(nddb)
    dispatcher = cherrypy.dispatch.RoutesDispatcher()

    # Dispatcher Connections

    #dispatcher.connect('meteor_get_all', '/meteors/', controller=m_con, action = 'GET_ALL', conditions=dict(method=['GET']))
    dispatcher.connect('meteor_get_geo', '/landslides/geo/:key', controller=lcon, action = 'GET_GEO', conditions=dict(method=['GET']))
    dispatcher.connect('meteor_get_year', '/landslides/year/:key', controller=lcon, action = 'GET_YEAR', conditions=dict(method=['GET']))
    dispatcher.connect('meteor_get_state', '/landslides/state/:key', controller=lcon, action = 'GET_STATE', conditions=dict(method=['GET']))

    # Configuration

    conf = {
            'global' : {
                'server.socket_host': 'student04.cse.nd.edu',
                'server.socket_port': 51092,
                },
            '/' : {'request.dispatch': dispatcher}
            }

    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf)
    cherrypy.quickstart(app)


# DRIVER

if __name__ == '__main__':
    start_service()

