#!/usr/bin/env python3

import cherrypy
import json
from _natural_disasters_database import _natural_disasters_database

class LandslideController(object):

    def __init__(self, nddb = None):
        if nddb is None:
            self.nddb = _natural_disasters_database
        else:
            self.nddb = nddb
        
        self.nddb.landslides.load_landslides('../landslides/landslide_formatted.csv');

    def GET_GEO(self, key):
        # Default Outut
        output = {'result': 'success'}
        geo_id = (key[0], key[1])

        # Try - except
        try:
            landslide = self.nddb.landslides.get_landslide(lat, lon)
            if landslide is not None:
                output['id']        = landslide[0]
                output['data']      = landslide[1]
                output['category']  = landslide[2]
                output['lat']       = landslide[3]
                output['lon']       = landslide[4]
            else:
                output['result']    = 'error'
                output['message']   = 'landslide not found'

        except Exception as ex:
            output['result']        = 'error'
            output['message']       = str(ex)

        return json.dumps(output)

    def GET_YEAR(self, key):
        # Default Outut
        output = {'result': 'success'}

        # Try - except
        try:
            landslide = self.nddb.landslides.get_landslide_year(key)
            if landslide is not None:
                output['id']        = landslide[0]
                output['data']      = landslide[1]
                output['category']  = landslide[2]
                output['lat']       = landslide[3]
                output['lon']       = landslide[4]
            else:
                output['result']    = 'error'
                output['message']   = 'landslide not found'

        except Exception as ex:
            output['result']        = 'error'
            output['message']       = str(ex)

        return json.dumps(output)

    def GET_STATE(self, key):
        # Default Outut
        output = {'result': 'success'}

        # Try - except
        try:
            landslide = self.nddb.landslides.get_landslide_state(key)
            if landslide is not None:
                output['id']        = landslide[0]
                output['data']      = landslide[1]
                output['category']  = landslide[2]
                output['lat']       = landslide[3]
                output['lon']       = landslide[4]
            else:
                output['result']    = 'error'
                output['message']   = 'landslide not found'

        except Exception as ex:
            output['result']        = 'error'
            output['message']       = str(ex)

        return json.dumps(output)
    '''
    def GET_ALL(self):

        output = dict()
        entries = [json.loads(self.GET_KEY(location)) for location in self.nddb.meteors.keys()]
    '''
