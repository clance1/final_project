// Creates the content for the Natural Disasters webpage
// main.js

// Checkboxes for disaster types
var fireLabel = new Label();
fireLabel.createLabel("Fires", "fireLabel", "label");

var fireCheckbox = new Checkbox();
fireCheckbox.createCheckbox("fireCheckbox", "checkboxes");

var tornadoLabel = new Label();
tornadoLabel.createLabel("Tornadoes", "tornadoLabel", "label");

var tornadoCheckbox = new Checkbox();
tornadoCheckbox.createCheckbox("tornadoCheckbox", "checkboxes");

var landslideLabel = new Label();
landslideLabel.createLabel("Landslides", "landslideLabel", "label");

var landslideCheckbox = new Checkbox();
landslideCheckbox.createCheckbox("landslideCheckbox", "checkboxes");

var meteorLabel = new Label();
meteorLabel.createLabel("Meteors", "meteorLabel", "label");

var meteorCheckbox = new Checkbox();
meteorCheckbox.createCheckbox("meteorCheckbox", "checkboxes");

var checkboxDivFire = new  Div();
checkboxDivFire. createDiv("checkboxDivFire", "col");
checkboxDivFire.item.setAttribute("class", "col-sm-3");
checkboxDivFire.addToDiv(fireLabel);
checkboxDivFire.addToDiv(fireCheckbox);

var checkboxDiv1 = new Div();
checkboxDiv1.createDiv("checkboxDiv1" , "checkbox");
checkboxDiv1.item.setAttribute("class", "col-sm-3");

var checkboxDiv2 = new Div();
checkboxDiv2.createDiv("checkboxDiv2" , "checkbox");
checkboxDiv2.item.setAttribute("class", "col-sm-3");

var checkboxDiv3 = new Div();
checkboxDiv3.createDiv("checkboxDiv3" , "checkbox");
checkboxDiv3.item.setAttribute("class", "col-sm-3");

var checkboxDiv4 = new Div();
checkboxDiv4.createDiv("checkboxDiv4" , "checkbox");
checkboxDiv4.item.setAttribute("class", "col-sm-3");

checkboxDiv1.addToDiv(fireLabel);
checkboxDiv1.addToDiv(fireCheckbox);
checkboxDiv2.addToDiv(landslideLabel);
checkboxDiv2.addToDiv(landslideCheckbox);
checkboxDiv3.addToDiv(meteorLabel);
checkboxDiv3.addToDiv(meteorCheckbox);
checkboxDiv4.addToDiv(tornadoLabel);
checkboxDiv4.addToDiv(tornadoCheckbox);

var checkboxRow = new  Div();
checkboxRow. createDiv("checkboxRow", "checkbox");
checkboxRow.item.setAttribute("class", "row");
checkboxRow.addToDiv(checkboxDiv1);
checkboxRow.addToDiv(checkboxDiv2);
checkboxRow.addToDiv(checkboxDiv3);
checkboxRow.addToDiv(checkboxDiv4);
checkboxRow.addToDocument();

// Input Text fields
var yearTextFieldLabel = new Label();
yearTextFieldLabel.createLabel("Year: ", "yearTextFieldLabel", "label");

var yearTextField = new TextField();
yearTextField.createTextField("tf_year");

var stateTextFieldLabel = new Label();
stateTextFieldLabel.createLabel("State: ", "stateTextFieldLabel", "label");

var stateTextField = new TextField();
stateTextField.createTextField("stateTextField");

var textFieldDiv = new Div();
textFieldDiv.createDiv("textFieldDiv", "textfield");
textFieldDiv.addToDiv(yearTextFieldLabel);
textFieldDiv.addToDiv(yearTextField);
textFieldDiv.addToDiv(stateTextFieldLabel);
textFieldDiv.addToDiv(stateTextField);

var display = new Button();
display.createButton("Display", "display", "display");
display.addClickEventHandler(displayHandler);

function displayHandler() {
    var showFire        = fireCheckbox.checkStatus();
    var showMeteor      = meteorCheckbox.checkStatus();
    var showLandslide   = landslideCheckbox.checkStatus();
    var showTornado     = tornadoCheckbox.checkStatus();

    var year            = document.getElementById('tf_year').value;
    var state           = document.getElementById('stateTextField').value;

    console.log("YEAR: " + year);
    console.log("STATE: " + state);
    clearOverlays();

    if (showLandslide) {
        getLandslides(year, state);
    }
    if (showFire) {
        getFires(year, state);
    }
    if (showMeteor) {
        getMeteors(year, state);
    }
    if (showTornado) {
        getTornadoes(year, state);
    }
}

var markersArray_l = [];
var infoArray_l    = [];
var markersArray_f = [];
var infoArray_f    = [];
var markersArray_t = [];
var infoArray_t    = [];
var markersArray_m = [];
var infoArray_m    = [];

function clearOverlays() {
    for (var i = 0; i < markersArray_l.length; i++ ) {
      markersArray_l[i].setMap(null);
    }
    markersArray_l.length = 0;
    infoArray_l.length = 0;

    for (var i = 0; i < markersArray_f.length; i++ ) {
      markersArray_f[i].setMap(null);
    }
    markersArray_f.length = 0;
    infoArray_f.length = 0;

    for (var i = 0; i < markersArray_t.length; i++ ) {
      markersArray_t[i].setMap(null);
    }
    markersArray_t.length = 0;
    infoArray_t.length = 0;

    for (var i = 0; i < markersArray_m.length; i++ ) {
      markersArray_m[i].setMap(null);
    }
    markersArray_m.length = 0;
    infoArray_m.length = 0;
  }

var kansas = {lat: 39.099728, lng: -94.578568};
var map;

var mapDiv = new Div();
mapDiv.createDiv("map", "map");

function initMap() {
    map = new google.maps.Map(
        document.getElementById('map'), {
          zoom: 4, center: kansas,
          mapTypeId: 'roadmap',
        },
    );
}

function createListener(type, marker, infoWindow, index) {

    if (type == "l") {
        markersArray_l.push(marker);
        infoArray_l.push(infoWindow);

        markersArray_l[index].addListener('click', function() {
            infoArray_l[index].open(map, markersArray_l[index]);
        });
    }

    else if (type == "f") {
        markersArray_f.push(marker);
        infoArray_f.push(infoWindow);

        markersArray_f[index].addListener('click', function() {
            infoArray_f[index].open(map, markersArray_f[index]);
        });
    }

    else if (type == "t") {
        markersArray_t.push(marker);
        infoArray_t.push(infoWindow);

        markersArray_t[index].addListener('click', function() {
            infoArray_t[index].open(map, markersArray_t[index]);
        });
    }

    else if (type == "m") {
        markersArray_m.push(marker);
        infoArray_m.push(infoWindow);

        markersArray_m[index].addListener('click', function() {
            infoArray_m[index].open(map, markersArray_m[index]);
        });
    }

}

function getLandslides(year, state) {
    var rec = new XMLHttpRequest();
    if (year == "") {
      rec.open("GET", "http://student04.cse.nd.edu:51092/landslides/state/" + state, true);
      console.log("Landslide url: " + "http://student04.cse.nd.edu:51092/landslides/state/" + state);
    }
    else {
      rec.open("GET", "http://student04.cse.nd.edu:51092/landslides/year/" + year, true);
      console.log("Landslide url: " + "http://student04.cse.nd.edu:51092/landslides/year/" + year);
    }
    rec.onload = function(e) {
        var landslides = JSON.parse(rec.responseText)["landslides"];
        for (var i = 0; i < landslides.length; i++) {
          var lat = landslides[i]["lat"];
          var lon = landslides[i]["lon"];

          var date = landslides[i]["date"];
          var cat  = landslides[i]["category"];

          var contentString = '<div id="content">'+
          '<div id="siteNotice">'+
          '</div>'+
          '<p>Type: Landslide</p>'+
          '<p>Date: ' + date + '</p>'+
          '<p>Category: ' + cat + '</p>'+
          '<p>Latitude: ' + lat + '</p>'+
          '<p>Longitude: ' + lon + '</p>'+
          '</div>'+
          '</div>';

          var infowindow = new google.maps.InfoWindow({
            content: contentString
          });

          var marker = new google.maps.Marker({
              position: {lat: parseFloat(lat), lng: parseFloat(lon)},
              map: map,
              icon: {
                url: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
              }
          });

          createListener("l", marker, infowindow, i);
        }
    }
    rec.onerror = function(e) {
        console.log(rec.statusText);
    }
    rec.send(null);
}

function getFires(year, state) {
    var rec = new XMLHttpRequest();
    if (year == "") {
      rec.open("GET", "http://student04.cse.nd.edu:51092/fires/state/" + state, true);
      console.log("Landslide url: " + "http://student04.cse.nd.edu:51092/fires/state/" + state);
    }
    else {
      rec.open("GET", "http://student04.cse.nd.edu:51092/fires/year/" + year, true);
      console.log("Landslide url: " + "http://student04.cse.nd.edu:51092/fires/year/" + year);
    }
    rec.onload = function(e) {
        var fires = JSON.parse(rec.responseText)['fires'];

        for (var i = 0; i < fires.length; i++) {
          var lat = fires[i]["latitude"];
          var lon = fires[i]["longitude"];

          var start = fires[i]["start_doy"];
          var end   = fires[i]["end_doy"];
          var cat   = fires[i]["category"];
          var y     = fires[i]["year"];

          var contentString = '<div id="content">'+
          '<div id="siteNotice">'+
          '</div>'+
          '<p>Type: Fire</p>'+
          '<p>Start_doy: ' + start + '</p>'+
          '<p>End_doy: ' + end + '</p>'+
          '<p>Category: ' + cat + '</p>'+
          '<p>Latitude: ' + lat + '</p>'+
          '<p>Longitude: ' + lon + '</p>'+
          '<p>Year: ' + y + '</p>' +
          '</div>'+
          '</div>';

          var infowindow = new google.maps.InfoWindow({
            content: contentString
          });

          var marker = new google.maps.Marker({
              position: {lat: parseFloat(lat), lng: parseFloat(lon)},
              map: map,
              icon: {
                url: "http://maps.google.com/mapfiles/ms/icons/red-dot.png"
              }
          });

          createListener("f", marker, infowindow, i);

        }
    }
    rec.onerror = function(e) {
        console.log(rec.statusText);
    }
    rec.send(null);
}

function getMeteors(year, state) {
    var rec = new XMLHttpRequest();
    if (year == "") {
      rec.open("GET", "http://student04.cse.nd.edu:51092/meteors/state/" + state, true);
      console.log("Landslide url: " + "http://student04.cse.nd.edu:51092/meteors/state/" + state);
    }
    else {
      rec.open("GET", "http://student04.cse.nd.edu:51092/meteors/year/" + year, true);
      console.log("Landslide url: " + "http://student04.cse.nd.edu:51092/meteors/year/" + year);
    }
    rec.onload = function(e) {
        var meteors = JSON.parse(rec.responseText)["meteors"];
        for (var i = 0; i < meteors.length; i++) {
          var lat = meteors[i]["latitude"];
          var lon = meteors[i]["longitude"];

          var name = meteors[i]["name"];
          var recclass = meteors[i]["recclass"];
          var mass = meteors[i]["mass"];
          var year = meteors[i]["year"];

          var contentString = '<div id="content">'+
          '<div id="siteNotice">'+
          '</div>'+
          '<p>Type: Meteor</p>'+
          '<p>Name: ' + name + '</p>'+
          '<p>Year: ' + year + '</p>'+
          '<p>Recclass: ' + recclass + '</p>'+
          '<p>Mass: ' + mass + '</p>'+

          '<p>Latitude: ' + lat + '</p>'+
          '<p>Longitude: ' + lon + '</p>'+
          '</div>'+
          '</div>';

          var infowindow = new google.maps.InfoWindow({
            content: contentString
          });

          var marker = new google.maps.Marker({
              position: {lat: parseFloat(lat), lng: parseFloat(lon)},
              map: map,
              icon: {
                url: "http://maps.google.com/mapfiles/ms/icons/purple-dot.png"
              },
          });

          createListener("m", marker, infowindow, i);
        }
    }
    rec.onerror = function(e) {
        console.log(rec.statusText);
    }
    rec.send(null);
}

function getTornadoes(year, state) {
    var rec = new XMLHttpRequest();
    if (year == "") {
      rec.open("GET", "http://student04.cse.nd.edu:51092/tornadoes/state/" + state, true);
      console.log("Landslide url: " + "http://student04.cse.nd.edu:51092/tornadoes/state/" + state);
    }
    else {
      rec.open("GET", "http://student04.cse.nd.edu:51092/tornadoes/year/" + year, true);
      console.log("Landslide url: " + "http://student04.cse.nd.edu:51092/tornadoes/year/" + year);
    }
    rec.onload = function(e) {
        var tornadoes = JSON.parse(rec.responseText)["tornadoes"];
        for (var i = 0; i < tornadoes.length; i++) {
          var lat = tornadoes[i]["latitude"];
          var lon = tornadoes[i]["longitude"];

          var ms = new google.maps.Marker({
              position: {lat: parseFloat(lat), lng: parseFloat(lon)},
              map: map,
              icon: {
                url: "http://maps.google.com/mapfiles/ms/icons/yellow-dot.png"
              },
          });

          markersArray_t.push(ms);
        }
    }
    rec.onerror = function(e) {
        console.log(rec.statusText);
    }
    rec.send(null);
}


// ALL OF THE FORMS FOR THE INPUTTING OF THE VALUES
// The only thing that needs to be added is the sending of the HTTP request
// also the selection of which form to appear

function createFire() {
  var fire_div = new Div();
  fire_div.createDiv("fire_div", "fire");

  var fire_label = new Label();
  fire_label.createLabel("FIRE INPUT FORM", "fire_label");
  var lat_label = new Label();
  lat_label.createLabel("Latitude:", "lat_label");
  var long_label = new Label();
  long_label.createLabel("Longitude:", "long_label");
  var start_label = new Label();
  start_label.createLabel("Start Day of Year:", "start_label");
  var end_label = new Label();
  end_label.createLabel("End Day of Year:", "end_label");
  var cat_label = new Label();
  cat_label.createLabel("Category:", "cat_label");
  var year_label = new Label();
  year_label.createLabel("Year:", "year_label");
  var state_label = new Label();
  state_label.createLabel("State Abreviation:", "state_label");

  // Create all the input fields with each item that needs
  // to be entered for the PUT methods
  var lat_in = new TextInput();
  lat_in.createTextInput("lat", "", "latitude");
  var long_in = new TextInput();
  long_in.createTextInput("long", "", "longitude");
  var start_in = new TextInput();
  start_in.createTextInput("start", "", "start_doy");
  var end_in = new TextInput();
  end_in.createTextInput("end", "", "end_doy");
  var cat_in = new TextInput();
  cat_in.createTextInput("cat", "", "category");
  var year_in = new TextInput();
  year_in.createTextInput("year", "", "year");
  var state_in = new TextInput();
  state_in.createTextInput("state", "", "state");

  var sub_button = new Button();
  sub_button.createButton("Submit", "submit");
  sub_button.addClickEventHandler(generateJSON_Fire);

  fire_div.appendChild(fire_label.item);
  fire_div.appendChild(lat_label.item);
  fire_div.appendChild(lat_in.item);
  fire_div.appendChild(long_label.item);
  fire_div.appendChild(long_in.item);
  fire_div.appendChild(start_label.item);
  fire_div.appendChild(start_in.item);
  fire_div.appendChild(end_label.item);
  fire_div.appendChild(end_in.item);
  fire_div.appendChild(cat_label.item);
  fire_div.appendChild(cat_in.item);
  fire_div.appendChild(year_label.item);
  fire_div.appendChild(year_in.item);
  fire_div.appendChild(state_label.item);
  fire_div.appendChild(state_in.item);
  fire_div.appendChild(sub_button.item);
  fire_div.addToDocument();
}

function createTornado() {
  var tornado_div = new Div();
  tornado_div.createDiv("tornado_div", "tornado");

  var fire_label = new Label();
  tornado_label.createLabel("tornado INPUT FORM", "tornado_label");
  var blat_label = new Label();
  blat_label.createLabel("Begin Latitude:", "blat_label");
  var blong_label = new Label();
  blong_label.createLabel("Begin Longitude:", "blong_label");
  var elat_label = new Label();
  elat_label.createLabel("Ending Latitude:", "elat_label");
  var elong_label = new Label();
  elong_label.createLabel("Ending Longitude:", "elong_label");
  var starty_label = new Label();
  starty_label.createLabel("Start Year:", "starty_label");
  var endy_label = new Label();
  endy_label.createLabel("End Year:", "endy_label");
  var startm_label = new Label();
  startm_label.createLabel("Start Month:", "startm_label");
  var endm_label = new Label();
  endm_label.createLabel("End Month:", "endm_label");
  var startd_label = new Label();
  startd_label.createLabel("Start Day:", "startd_label");
  var endd_label = new Label();
  endd_label.createLabel("End Day:", "endd_label");
  var startt_label = new Label();
  startt_label.createLabel("Start Time:", "startt_label");
  var endt_label = new Label();
  endt_label.createLabel("End Time:", "endt_label");
  var event_label = new Label();
  event_label.createLabel("Event ID:", "event_label");
  var cz_label = new Label();
  cz_label.createLabel("CZ Name:", "cz_label");
  var state_label = new Label();
  state_label.createLabel("State Abreviation:", "state_label");
  var scale_label = new Label();
  scale_label.createLabel("Scale:", "scale_label");

  // Create all the input fields with each item that needs
  // to be entered for the PUT methods
  var latb_in = new TextInput();
  latb_in.createTextInput("begin_lat", "", "begin_latitude");
  var longb_in = new TextInput();
  longb_in.createTextInput("begin_long", "", "begin_longitude");
  var late_in = new TextInput();
  late_in.createTextInput("end_lat", "", "end_latitude");
  var longe_in = new TextInput();
  longe_in.createTextInput("end_long", "", "end_longitude");
  var starty_in = new TextInput();
  starty_in.createTextInput("begin_year", "", "start_year");
  var endy_in = new TextInput();
  endy_in.createTextInput("end_year", "", "end_year");
  var startm_in = new TextInput();
  startm_in.createTextInput("begin_month", "", "start_month");
  var endm_in = new TextInput();
  endm_in.createTextInput("end_month", "", "end_month");
  var startd_in = new TextInput();
  startd_in.createTextInput("begin_day", "", "start_day");
  var endd_in = new TextInput();
  endd_in.createTextInput("end_day", "", "end_day");
  var starty_in = new TextInput();
  startt_in.createTextInput("begin_time", "", "start_time");
  var endt_in = new TextInput();
  endt_in.createTextInput("end_time", "", "end_time");
  var cz_in = new TextInput();
  cz_in.createTextInput("cz_name", "", "cz_name");
  var event_in = new TextInput();
  event_in.createTextInput("event_id", "", "event_id");
  var scale_in = new TextInput();
  scale_in.createTextInput("tor_F_scale", "", "scale_id");
  var state_in = new TextInput();
  state_in.createTextInput("state", "", "state");

  var sub_button = new Button();
  sub_button.createButton("Submit", "submit");
  sub_button.addClickEventHandler(generateJSON_Tornado);

  var linebreak = document.createElement("br");

  // Add all the elements to the document
  tornado_div.appendChild(tornado_label.item);
  tornado_div.appendChild(blat_label.item);
  tornado_div.appendChild(latb_in.item);
  tornado_div.appendChild(elat_label.item);
  tornado_div.appendChild(late_in.item);
  tornado_div.appendChild(linebreak);
  tornado_div.appendChild(blong_label.item);
  tornado_div.appendChild(longb_in.item);
  tornado_div.appendChild(elong_label.item);
  tornado_div.appendChild(longe_in.item);

  tornado_div.appendChild(starty_label.item);
  tornado_div.appendChild(starty_in.item);
  tornado_div.appendChild(startm_label.item);
  tornado_div.appendChild(startm_in.item);
  tornado_div.appendChild(startd_label.item);
  tornado_div.appendChild(startd_in.item);
  tornado_div.appendChild(startt_label.item);
  tornado_div.appendChild(startt_in.item);

  tornado_div.appendChild(endy_label.item);
  tornado_div.appendChild(endy_in.item);
  tornado_div.appendChild(endm_label.item);
  tornado_div.appendChild(endm_in.item);
  tornado_div.appendChild(endd_label.item);
  tornado_div.appendChild(endd_in.item);
  tornado_div.appendChild(endt_label.item);
  tornado_div.appendChild(endt_in.item);

  tornado_div.appendChild(event_label.item);
  tornado_div.appendChild(event_in.item);

  tornado_div.appendChild(cz_label.item);
  tornado_div.appendChild(cz_in.item);

  tornado_div.appendChild(scale_label.item);
  tornado_div.appendChild(scale_in.item);

  tornado_div.appendChild(state_label.item);
  tornado_div.appendChild(state_in.item);

  tornado_div.appendChild(sub_button.item);
  tornado_div.appendChild(linebreak);
  tornado_div.addToDocument();
}

function createLandslide() {
  var landslide_div = new Div();
  landslide_div.createDiv("landslide_div", "landslide");

  var landslide_label = new Label();
  landslide_label.createLabel("LANDLSIDE INPUT FORM", "landslide_label");
  var lat_label = new Label();
  lat_label.createLabel("Latitude:", "lat_label");
  var long_label = new Label();
  long_label.createLabel("Longitude:", "long_label");
  var date_label = new Label();
  date_label.createLabel("Date:", "start_label");
  var cat_label = new Label();
  cat_label.createLabel("Category:", "cat_label");
  var year_label = new Label();
  year_label.createLabel("Year:", "year_label");
  var state_label = new Label();
  state_label.createLabel("State Abreviation:", "state_label");

  // Create all the input fields with each item that needs
  // to be entered for the PUT methods
  var lat_in = new TextInput();
  lat_in.createTextInput("lat", "", "latitude");
  var long_in = new TextInput();
  long_in.createTextInput("lon", "", "longitude");
  var date_in = new TextInput();
  date_in.createTextInput("date", "", "date");
  var cat_in = new TextInput();
  cat_in.createTextInput("cat", "", "category");
  var year_in = new TextInput();
  year_in.createTextInput("year", "", "year");
  var state_in = new TextInput();
  state_in.createTextInput("state", "", "state");

  var sub_button = new Button();
  sub_button.createButton("Submit", "submit");
  sub_button.addClickEventHandler(generateJSON_Landslide);

  landslide_div.appendChild(landslide_label.item);
  landslide_div.appendChild(lat_label.item);
  landslide_div.appendChild(lat_in.item);
  landslide_div.appendChild(long_label.item);
  landslide_div.appendChild(long_in.item);
  landslide_div.appendChild(date_label.item);
  landslide_div.appendChild(date_in.item);
  landslide_div.appendChild(cat_label.item);
  landslide_div.appendChild(cat_in.item);
  landslide_div.appendChild(year_label.item);
  landslide_div.appendChild(year_in.item);
  landslide_div.appendChild(state_label.item);
  landslide_div.appendChild(state_in.item);
  landslide_div.appendChild(sub_button.item);
  landslide_div.addToDocument();
}

function createMeteor() {
  var meteor_div = new Div();
  meteor_div.createDiv("meteor_div", "meteor");

  var meteor_label = new Label();
  meteor_label.createLabel("METEOR INPUT FORM", "meteor_label");
  var lat_label = new Label();
  lat_label.createLabel("Latitude:", "lat_label");
  var long_label = new Label();
  long_label.createLabel("Longitude:", "long_label");
  var name_label = new Label();
  name_label.createLabel("Name:", "name_label");
  var nametype_label = new Label();
  nametype_label.createLabel("Name Type:", "nametype_label");
  var recclass_label = new Label();
  recclass_label.createLabel("Rec Class:", "recclass_label");
  var mass_label = new Label();
  mass_label.createLabel("Mass:", "mass_label");
  var fall_label = new Label();
  fall_label.createLabel("Fall:", "fall_label");
  var year_label = new Label();
  year_label.createLabel("Year:", "year_label");
  var state_label = new Label();
  state_label.createLabel("State Abreviation:", "state_label");

  // Create all the input fields with each item that needs
  // to be entered for the PUT methods
  var lat_in = new TextInput();
  lat_in.createTextInput("latitude", "", "latitude");
  var long_in = new TextInput();
  long_in.createTextInput("longitude", "", "longitude");
  var name_in = new TextInput();
  name_in.createTextInput("name", "", "name");
  var nametype_in = new TextInput();
  nametype_in.createTextInput("nametype", "", "nametype");
  var recclass_in = new TextInput();
  recclass_in.createTextInput("recclass", "", "recclass");
  var mass_in = new TextInput();
  mass_in.createTextInput("mass", "", "mass");
  var fall_in = new TextInput();
  fall_in.createTextInput("fall", "", "fall");
  var year_in = new TextInput();
  year_in.createTextInput("year", "", "year");
  var state_in = new TextInput();
  state_in.createTextInput("state", "", "state");

  var sub_button = new Button();
  sub_button.createButton("Submit", "submit");
  sub_button.addClickEventHandler(generateJSON_Meteor);

  meteor_div.appendChild(meteor_label.item);
  meteor_div.appendChild(lat_label.item);
  meteor_div.appendChild(lat_in.item);
  meteor_div.appendChild(long_label.item);
  meteor_div.appendChild(long_in.item);
  meteor_div.appendChild(name_label.item);
  meteor_div.appendChild(name_in.item);
  meteor_div.appendChild(nametype_label.item);
  meteor_div.appendChild(nametype_in.item);
  meteor_div.appendChild(recclass_label.item);
  meteor_div.appendChild(recclass_in.item);
  meteor_div.appendChild(mass_label.item);
  meteor_div.appendChild(mass_in.item);
  meteor_div.appendChild(fall_label.item);
  meteor_div.appendChild(fall_in.item);
  meteor_div.appendChild(year_label.item);
  meteor_div.appendChild(year_in.item);
  meteor_div.appendChild(state_label.item);
  meteor_div.appendChild(state_in.item);
  meteor_div.appendChild(sub_button.item);
  meteor_div.addToDocument();
}

function generateJSON_Tornado() {
  var js = {'begin_lat': document.getElementById("begin_lat").value,
            'end_lat': document.getElementById("end_lat").value,
            'begin_long': document.getElementById("begin_lat").value,
            'end_long': document.getElementById("end_lat").value,
            'begin_year': document.getElementById("begin_year").value,
            'begin_month': document.getElementById("begin_month").value,
            'begin_day': document.getElementById("begin_day").value,
            'begin_time': document.getElementById("begin_time").value,
            'end_year': document.getElementById("end_year").value,
            'end_month': document.getElementById("end_month").value,
            'end_day': document.getElementById("end_day").value,
            'end_time': document.getElementById("end_time").value,
            'event_id': document.getElementById("event_id").value,
            'cz_name': document.getElementById("cz_name").value,
            'tor_F_scale': document.getElementById("tor_F_scale").value,
            'state': document.getElementById("state").value}
  text = JSON.stringify(js);
  alert(text);
}

function generateJSON_Fire() {
  var js = {'latitude': document.getElementById("lat").value,
            'longitude': document.getElementById("long").value,
            'start_doy': document.getElementById("start").value,
            'end_doy': document.getElementById("end").value,
            'category': document.getElementById("cat").value,
            'year': document.getElementById("year").value,
            'state': document.getElementById("state").value}
  text = JSON.stringify(js);
  alert(text);
}

function generateJSON_Landslide() {
  var js = {'lat': document.getElementById("lat").value,
            'lon': document.getElementById("lon").value,
            'date': document.getElementById("date").value,
            'category': document.getElementById("cat").value,
            'year': document.getElementById("year").value,
            'state': document.getElementById("state").value}
  text = JSON.stringify(js);
  alert(text);
}

function generateJSON_Meteor() {
  var js = {'latitude': document.getElementById("latitude").value,
            'longitude': document.getElementById("longitude").value,
            'name': document.getElementById("name").value,
            'nametype': document.getElementById("nametype").value,
            'recclass': document.getElementById("recclass").value,
            'fall': document.getElementById("fall").value,
            'mass': document.getElementById("mass").value,
            'year': document.getElementById("year").value,
            'state': document.getElementById("state").value}
  text = JSON.stringify(js);
  alert(text);
}
