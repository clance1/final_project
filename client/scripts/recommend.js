
var description = new Label();
var des = "In order to further analyze our data we implemented a Density-Based Spatial Clustering of Applications with Noise (DBSCAN) algorithm. The basis of DBSCAN is that it will parse the data and take certain points of density based on a minimum requirement and search for values within a certain distance away from the center cluster. In addition if a cluster is within the specified distance away from another cluster center then will be combined into one cluster. The values that are not within the specified distance become outliers. In the context of our project we are using the clustering data to check if certain places represented by our data are simply freak occurances or if they are consistently being affected by natural disasters. Using this data the user can decide whether or not to live in a certain area. The only scans that are shown below are those that have more than 50 data objects in a given year."
description.createLabel(des, "description", "label");

var d1 = new Dropdown();
d1.createDropdown(["Fire 2000", "Fire 2005", "Fire 2006", "Fire 2007",  "Fire 2008", "Fire 2009", "Landslide 2008", "Landslide 2010", "Landslide 2011", "Landslide 2013", "Landslide 2014", "Tornado 2019", "All Fires", "All Landslides", "All Tornadoes"], "dropdown1");
d1.addToDocument();

var display = new Button();
display.createButton("Display", "display", "display");
display.addClickEventHandler(displayHandler);

var imgMain = new Img();
imgMain.createImg("imgMain", "cluster_img", "images/meme.png");

function displayHandler() {
  	var selection = document.getElementById("dropdown1").value;
    console.log(selection);
    if (selection == 0) {
      imgMain.setLink("images/clusters/fire2000.png");
    }
    else if (selection == 1) {
      imgMain.setLink("images/clusters/fire2005.png");
    }
    else if (selection == 2) {
      imgMain.setLink("images/clusters/fire2006.png");
    }
    else if (selection == 3) {
      imgMain.setLink("images/clusters/fire2007.png");
    }
    else if (selection == 4) {
      imgMain.setLink("images/clusters/fire2008.png");
    }
    else if (selection == 5) {
      imgMain.setLink("images/clusters/fire2009.png");
    }
    else if (selection == 6) {
      imgMain.setLink("images/clusters/land2008.png");
    }
    else if (selection == 7) {
      imgMain.setLink("images/clusters/land2010.png");
    }
    else if (selection == 8) {
      imgMain.setLink("images/clusters/land2011.png");
    }
    else if (selection == 9) {
      imgMain.setLink("images/clusters/land2013.png");
    }
    else if (selection == 10) {
      imgMain.setLink("images/clusters/land2014.png");
    }
    else if (selection == 11) {
      imgMain.setLink("images/clusters/torn2019.png");
    }
    else if (selection == 12) {
      imgMain.setLink("images/fire_cluster.png");
    }
    else if (selection == 13) {
      imgMain.setLink("images/landslide_cluster.png");
    }
    else if (selection == 14) {
      imgMain.setLink("images/tornado_cluster.png");
    }
}
