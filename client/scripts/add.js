// Creates the content for the page to add disasters

// Dropdown to select disaster type
Label.prototype = new Item();
var dropdownLabel = new Label();
dropdownLabel.createLabel("Select event to add:", "dropdownLabel");
dropdownLabel.addToDocument();

var fire_button = new Button();
fire_button.createButton("Fire", "fire", "fire_button");
fire_button.addClickEventHandler(display, "fire");

var tornado_button = new Button();
tornado_button.createButton("Tornado", "tornado", "tornado_button");
tornado_button.addClickEventHandler(display, "tornado");

var landslide_button = new Button();
landslide_button.createButton("Landslide", "landslide", "landslide_button");
landslide_button.addClickEventHandler(display, "landslide");

var meteor_button = new Button();
meteor_button.createButton("Meteor", "meteor", "meteor_button");
meteor_button.addClickEventHandler(display, "meteor");

/*
var disasters = {0: "", 1: "Fire", 2:"Landslides", 3 : "Meteor", 4 : "Tornado"};
Dropdown.prototype = new Item();
var dropdown = new Dropdown();
dropdown.createDropdown(disasters, "dropdown", "fire");


dropdown.onclick = display(dropdown.getSelected());
dropdown.addToDocument();
*/
// generates the appropriate input boxes based on the disaster added

function display(event){
    console.log("display called");
    console.log(event);

    TextField.prototype = new Item();
    Div.prototype = new Item();
    GamepadButton.prototype = new Item();

    if (event == "fire"){
      removeAll();

      var blankLabel = new Label();
      blankLabel.createLabel("", "blankLabel");

      var fire_div = new Div();
      fire_div.createDiv("fire_div", "fire");

      var fire_label = new Label();
      fire_label.createLabel("FIRE INPUT FORM", "fire_label");
      var lat_label = new Label();
      lat_label.createLabel("Latitude:", "lat_label");
      var long_label = new Label();
      long_label.createLabel("Longitude:", "long_label");
      var start_label = new Label();
      start_label.createLabel("Start Day of Year:", "start_label");
      var end_label = new Label();
      end_label.createLabel("End Day of Year:", "end_label");
      var cat_label = new Label();
      cat_label.createLabel("Category:", "cat_label");
      var year_label = new Label();
      year_label.createLabel("Year:", "year_label");
      var state_label = new Label();
      state_label.createLabel("State Abreviation:", "state_label");

      // Create all the input fields with each item that needs
      // to be entered for the PUT methods
      var lat_in = new TextInput();
      lat_in.createTextInput("lat", "", "latitude");
      var long_in = new TextInput();
      long_in.createTextInput("long", "", "longitude");
      var start_in = new TextInput();
      start_in.createTextInput("start", "", "start_doy");
      var end_in = new TextInput();
      end_in.createTextInput("end", "", "end_doy");
      var cat_in = new TextInput();
      cat_in.createTextInput("cat", "", "category");
      var year_in = new TextInput();
      year_in.createTextInput("year", "", "year");
      var state_in = new TextInput();
      state_in.createTextInput("state", "", "state");

      var sub_button = new Button();
      sub_button.createButton("Submit", "submit", "submit");
      sub_button.addClickEventHandler(generateJSON_Fire);

      fire_div.addToDiv(fire_label);
      fire_div.addToDiv(lat_label);
      fire_div.addToDiv(lat_in);
      fire_div.addToDiv(long_label);
      fire_div.addToDiv(long_in);
      fire_div.addToDiv(start_label);
      fire_div.addToDiv(start_in);
      fire_div.addToDiv(end_label);
      fire_div.addToDiv(end_in);
      fire_div.addToDiv(cat_label);
      fire_div.addToDiv(cat_in);
      fire_div.addToDiv(year_label);
      fire_div.addToDiv(year_in);
      fire_div.addToDiv(state_label);
      fire_div.addToDiv(state_in);
      fire_div.addToDiv(blankLabel);
      fire_div.addToDiv(sub_button);
      fire_div.addToDocument();
    }
    else if (event == "landslide"){
      removeAll();

      var blankLabel = new Label();
      blankLabel.createLabel("", "blankLabel");

      var landslide_div = new Div();
      landslide_div.createDiv("landslide_div", "landslide");

      var landslide_label = new Label();
      landslide_label.createLabel("LANDLSIDE INPUT FORM", "landslide_label");
      var lat_label = new Label();
      lat_label.createLabel("Latitude:", "lat_label");
      var long_label = new Label();
      long_label.createLabel("Longitude:", "long_label");
      var date_label = new Label();
      date_label.createLabel("Date:", "start_label");
      var cat_label = new Label();
      cat_label.createLabel("Category:", "cat_label");
      var year_label = new Label();
      year_label.createLabel("Year:", "year_label");
      var state_label = new Label();
      state_label.createLabel("State Abreviation:", "state_label");

      // Create all the input fields with each item that needs
      // to be entered for the PUT methods
      var lat_in = new TextInput();
      lat_in.createTextInput("lat", "", "latitude");
      var long_in = new TextInput();
      long_in.createTextInput("lon", "", "longitude");
      var date_in = new TextInput();
      date_in.createTextInput("date", "", "date");
      var cat_in = new TextInput();
      cat_in.createTextInput("cat", "", "category");
      var year_in = new TextInput();
      year_in.createTextInput("year", "", "year");
      var state_in = new TextInput();
      state_in.createTextInput("state", "", "state");

      var sub_button = new Button();
      sub_button.createButton("Submit", "submit", "submit");
      sub_button.addClickEventHandler(generateJSON_Landslide);

      landslide_div.addToDiv(landslide_label);
      landslide_div.addToDiv(lat_label);
      landslide_div.addToDiv(lat_in);
      landslide_div.addToDiv(long_label);
      landslide_div.addToDiv(long_in);
      landslide_div.addToDiv(date_label);
      landslide_div.addToDiv(date_in);
      landslide_div.addToDiv(cat_label);
      landslide_div.addToDiv(cat_in);
      landslide_div.addToDiv(year_label);
      landslide_div.addToDiv(year_in);
      landslide_div.addToDiv(state_label);
      landslide_div.addToDiv(state_in);
      landslide_div.addToDiv(blankLabel);
      landslide_div.addToDiv(sub_button);
      landslide_div.addToDocument();
    }
    else if (event == "meteor"){
        removeAll();

        var blankLabel = new Label();
        blankLabel.createLabel("", "blankLabel");
        // Meteor
        var meteor_div = new Div();
        meteor_div.createDiv("meteor_div", "meteor");

        var meteor_label = new Label();
        meteor_label.createLabel("METEOR INPUT FORM", "meteor_label");
        var lat_label = new Label();
        lat_label.createLabel("Latitude:", "lat_label");
        var long_label = new Label();
        long_label.createLabel("Longitude:", "long_label");
        var name_label = new Label();
        name_label.createLabel("Name:", "name_label");
        var nametype_label = new Label();
        nametype_label.createLabel("Name Type:", "nametype_label");
        var recclass_label = new Label();
        recclass_label.createLabel("Rec Class:", "recclass_label");
        var mass_label = new Label();
        mass_label.createLabel("Mass:", "mass_label");
        var fall_label = new Label();
        fall_label.createLabel("Fall:", "fall_label");
        var year_label = new Label();
        year_label.createLabel("Year:", "year_label");
        var state_label = new Label();
        state_label.createLabel("State Abreviation:", "state_label");

        // Create all the input fields with each item that needs
        // to be entered for the PUT methods
        var lat_in = new TextInput();
        lat_in.createTextInput("latitude", "", "latitude");
        var long_in = new TextInput();
        long_in.createTextInput("longitude", "", "longitude");
        var name_in = new TextInput();
        name_in.createTextInput("name", "", "name");
        var nametype_in = new TextInput();
        nametype_in.createTextInput("nametype", "", "nametype");
        var recclass_in = new TextInput();
        recclass_in.createTextInput("recclass", "", "recclass");
        var mass_in = new TextInput();
        mass_in.createTextInput("mass", "", "mass");
        var fall_in = new TextInput();
        fall_in.createTextInput("fall", "", "fall");
        var year_in = new TextInput();
        year_in.createTextInput("year", "", "year");
        var state_in = new TextInput();
        state_in.createTextInput("state", "", "state");

        var sub_button = new Button();
        sub_button.createButton("Submit", "submit", "submit");
        sub_button.addClickEventHandler(generateJSON_Meteor);

        meteor_div.addToDiv(meteor_label);
        meteor_div.addToDiv(lat_label);
        meteor_div.addToDiv(lat_in);
        meteor_div.addToDiv(long_label);
        meteor_div.addToDiv(long_in);
        meteor_div.addToDiv(name_label);
        meteor_div.addToDiv(name_in);
        meteor_div.addToDiv(nametype_label);
        meteor_div.addToDiv(nametype_in);
        meteor_div.addToDiv(recclass_label);
        meteor_div.addToDiv(recclass_in);
        meteor_div.addToDiv(mass_label);
        meteor_div.addToDiv(mass_in);
        meteor_div.addToDiv(fall_label);
        meteor_div.addToDiv(fall_in);
        meteor_div.addToDiv(year_label);
        meteor_div.addToDiv(year_in);
        meteor_div.addToDiv(state_label);
        meteor_div.addToDiv(state_in);
        meteor_div.addToDiv(blankLabel);
        meteor_div.addToDiv(sub_button);
        meteor_div.addToDocument();
    }
    else if (event == "tornado"){
        removeAll();

        var blankLabel = new Label();
        blankLabel.createLabel("", "blankLabel");
        // Tornado
        var tornado_div = new Div();
        tornado_div.createDiv("tornado_div", "tornado");

        var tornado_label = new Label();
        tornado_label.createLabel("TORNADO INPUT FORM", "tornado_label");
        var blat_label = new Label();
        blat_label.createLabel("Begin Latitude:", "blat_label");
        var blong_label = new Label();
        blong_label.createLabel("Begin Longitude:", "blong_label");
        var elat_label = new Label();
        elat_label.createLabel("Ending Latitude:", "elat_label");
        var elong_label = new Label();
        elong_label.createLabel("Ending Longitude:", "elong_label");
        var starty_label = new Label();
        starty_label.createLabel("Start Year:", "starty_label");
        var endy_label = new Label();
        endy_label.createLabel("End Year:", "endy_label");
        var startm_label = new Label();
        startm_label.createLabel("Start Month:", "startm_label");
        var endm_label = new Label();
        endm_label.createLabel("End Month:", "endm_label");
        var startd_label = new Label();
        startd_label.createLabel("Start Day:", "startd_label");
        var endd_label = new Label();
        endd_label.createLabel("End Day:", "endd_label");
        var startt_label = new Label();
        startt_label.createLabel("Start Time:", "startt_label");
        var endt_label = new Label();
        endt_label.createLabel("End Time:", "endt_label");
        var event_label = new Label();
        event_label.createLabel("Event ID:", "event_label");
        var cz_label = new Label();
        cz_label.createLabel("CZ Name:", "cz_label");
        var state_label = new Label();
        state_label.createLabel("State Abreviation:", "state_label");
        var scale_label = new Label();
        scale_label.createLabel("Scale:", "scale_label");

        // Create all the input fields with each item that needs
        // to be entered for the PUT methods
        var latb_in = new TextInput();
        latb_in.createTextInput("begin_lat", "", "begin_latitude");
        var longb_in = new TextInput();
        longb_in.createTextInput("begin_long", "", "begin_longitude");
        var late_in = new TextInput();
        late_in.createTextInput("end_lat", "", "end_latitude");
        var longe_in = new TextInput();
        longe_in.createTextInput("end_long", "", "end_longitude");
        var starty_in = new TextInput();
        starty_in.createTextInput("begin_year", "", "start_year");
        var endy_in = new TextInput();
        endy_in.createTextInput("end_year", "", "end_year");
        var startm_in = new TextInput();
        startm_in.createTextInput("begin_month", "", "start_month");
        var endm_in = new TextInput();
        endm_in.createTextInput("end_month", "", "end_month");
        var startd_in = new TextInput();
        startd_in.createTextInput("begin_day", "", "start_day");
        var endd_in = new TextInput();
        endd_in.createTextInput("end_day", "", "end_day");
        var startt_in = new TextInput();
        startt_in.createTextInput("begin_time", "", "start_time");
        var endt_in = new TextInput();
        endt_in.createTextInput("end_time", "", "end_time");
        var cz_in = new TextInput();
        cz_in.createTextInput("cz_name", "", "cz_name");
        var event_in = new TextInput();
        event_in.createTextInput("event_id", "", "event_id");
        var scale_in = new TextInput();
        scale_in.createTextInput("tor_F_scale", "", "scale_id");
        var state_in = new TextInput();
        state_in.createTextInput("state", "", "state");

        var sub_button = new Button();
        sub_button.createButton("Submit", "submit", "submit");
        sub_button.addClickEventHandler(generateJSON_Tornado);

        var linebreak = document.createElement("br");

        // Add all the elements to the document
        tornado_div.addToDiv(tornado_label);
        tornado_div.addToDiv(blat_label);
        tornado_div.addToDiv(latb_in);
        tornado_div.addToDiv(elat_label);
        tornado_div.addToDiv(late_in);
        tornado_div.addToDiv(blong_label);
        tornado_div.addToDiv(longb_in);
        tornado_div.addToDiv(elong_label);
        tornado_div.addToDiv(longe_in);

        tornado_div.addToDiv(starty_label);
        tornado_div.addToDiv(starty_in);
        tornado_div.addToDiv(startm_label);
        tornado_div.addToDiv(startm_in);
        tornado_div.addToDiv(startd_label);
        tornado_div.addToDiv(startd_in);
        tornado_div.addToDiv(startt_label);
        tornado_div.addToDiv(startt_in);

        tornado_div.addToDiv(endy_label);
        tornado_div.addToDiv(endy_in);
        tornado_div.addToDiv(endm_label);
        tornado_div.addToDiv(endm_in);
        tornado_div.addToDiv(endd_label);
        tornado_div.addToDiv(endd_in);
        tornado_div.addToDiv(endt_label);
        tornado_div.addToDiv(endt_in);

        tornado_div.addToDiv(event_label);
        tornado_div.addToDiv(event_in);

        tornado_div.addToDiv(cz_label);
        tornado_div.addToDiv(cz_in);

        tornado_div.addToDiv(scale_label);
        tornado_div.addToDiv(scale_in);

        tornado_div.addToDiv(state_label);
        tornado_div.addToDiv(state_in);

        tornado_div.addToDiv(blankLabel);
        tornado_div.addToDiv(sub_button);
        tornado_div.addToDocument();
    }

}


function generateJSON_Tornado() {
  var js = {'begin_lat': document.getElementById("begin_lat").value,
            'end_lat': document.getElementById("end_lat").value,
            'begin_long': document.getElementById("begin_lat").value,
            'end_long': document.getElementById("end_lat").value,
            'begin_year': document.getElementById("begin_year").value,
            'begin_month': document.getElementById("begin_month").value,
            'begin_day': document.getElementById("begin_day").value,
            'begin_time': document.getElementById("begin_time").value,
            'end_year': document.getElementById("end_year").value,
            'end_month': document.getElementById("end_month").value,
            'end_day': document.getElementById("end_day").value,
            'end_time': document.getElementById("end_time").value,
            'event_id': document.getElementById("event_id").value,
            'cz_name': document.getElementById("cz_name").value,
            'tor_F_scale': document.getElementById("tor_F_scale").value,
            'state': document.getElementById("state").value}
  text = JSON.stringify(js);
  postData(text, "tornadoes/");
  alert(text);
}

function generateJSON_Fire() {
  var js = {'latitude': document.getElementById("lat").value,
            'longitude': document.getElementById("long").value,
            'start_doy': document.getElementById("start").value,
            'end_doy': document.getElementById("end").value,
            'category': document.getElementById("cat").value,
            'year': document.getElementById("year").value,
            'state': document.getElementById("state").value}
  text = JSON.stringify(js);
  postData(text, "fires/");
  alert(text);
}

function generateJSON_Landslide() {
  var js = {'lat': document.getElementById("lat").value,
            'lon': document.getElementById("lon").value,
            'date': document.getElementById("date").value,
            'category': document.getElementById("cat").value,
            'year': document.getElementById("year").value,
            'state': document.getElementById("state").value}
  text = JSON.stringify(js);
  postData(text, "landslides/");
  alert(text);
}

function generateJSON_Meteor() {
  var js = {'latitude': document.getElementById("latitude").value,
            'longitude': document.getElementById("longitude").value,
            'name': document.getElementById("name").value,
            'nametype': document.getElementById("nametype").value,
            'recclass': document.getElementById("recclass").value,
            'fall': document.getElementById("fall").value,
            'mass': document.getElementById("mass").value,
            'year': document.getElementById("year").value,
            'state': document.getElementById("state").value}
  text = JSON.stringify(js);
  postData(text, "meteors/");
  alert(text);
}

function postData(dict, disaster) {
  var url = "http://student04.cse.nd.edu:51092/";
  url = url.concat(disaster);

  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, true);
  xhr.setRequestHeader('Content-type', 'text/plain; charset=utf-8');

  xhr.onload = function(e) {
    console.log(xhr.responseText);
  }

  xhr.onerror = function(e) {
    console.error(xhr.statusText);
  }

  xhr.send(dict);

}

// Cleans the page of all of the other options
function removeAll() {
  try {
    document.getElementById("tornado_div").remove();
  }
  catch(err) {
    console.log(err.message);
  }

  try {
    document.getElementById("fire_div").remove();
  }
  catch(err) {
    console.log(err.message);
  }

  try {
    document.getElementById("meteor_div").remove();
  }
  catch(err) {
    console.log(err.message);
  }

  try {
    document.getElementById("landslide_div").remove();
  }
  catch(err) {
    console.log(err.message);
  }
}