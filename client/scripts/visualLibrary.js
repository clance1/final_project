// label
function Item () {
    this.addToDocument = function() {
        document.body.appendChild(this.item);
    }
}

function Label() {
    this.createLabel = function(text, id, class_name) {
        this.item = document.createElement("p");
        this.item.setAttribute("id", id);
        this.item.setAttribute("class", class_name);
        this.setText(text);
        this.addToDocument();
    }

    this.setText = function(text) {
        this.item.innerHTML = text;
    }
}

Label();
Label.prototype = new Item();

// button
function Button() {
    this.createButton = function(text, id, class_name) {
        this.item = document.createElement("button");
        this.item.setAttribute("id", id);
        this.item.setAttribute("class", class_name);
        var itemText = document.createTextNode(text);
        this.item.appendChild(itemText);
        this.addToDocument();
    }

    this.addClickEventHandler = function(handler, args) {
        this.item.onmouseup = function() {
            handler(args);
        }
    }
}

Button();
Button.prototype = new Item();

// dropdown
function Dropdown() {
    this.createDropdown = function(dict, id, selected) {
        this.item = document.createElement("select");
        this.item.setAttribute("id", id);

        for (var key in dict) {
            listItem = document.createElement("option");
            listItem.setAttribute("value", key);
            listItem.innerHTML = dict[key];
            this.item.appendChild(listItem);
        }

        this.addToDocument();
    }

    this.getSelected = function() {
        e = this.item;
        var result = e.options[e.selectedIndex].value;
        console.log("CHOICE: " + result);
        return result;
    }
}
Dropdown();
Dropdown.prototype = new Item();

function Div() {
    this.createDiv = function(id, class_name) {
        this.item = document.createElement("div");
        this.item.setAttribute("id", id);
        this.item.setAttribute("class", class_name);
        this.addToDocument();
    }

    this.addToDiv = function(child) {
        this.item.appendChild(child.item);
    }

    this.remove = function() {
      this.parentElement.removeChild(this);
    }
}
Div();
Div.prototype = new Item();


function Img() {
    this.createImg = function(id, class_name, link) {
        this.item = document.createElement("img");
        this.item.setAttribute("id", id);
        this.item.setAttribute("class", class_name);
        this.item.setAttribute("src", link);
        this.addToDocument();
    }

    this.setLink = function(link) {
        this.item.setAttribute("src", link);
    }
}
Img();
Img.prototype = new Item();


function Checkbox() {
    this.createCheckbox = function(id, c){
        this.item = document.createElement("input");
        this.item.setAttribute("type", "checkbox");
        this.item.setAttribute("id", id);
        this.item.setAttribute("checked", false);
        this.item.setAttribute("class", c);
        this.addToDocument();
    }
    this.checkStatus = function() {
        return this.item.checked == true;
    }
}
Checkbox();
Checkbox.prototype = new Item();


function TextField() {
	this.createTextField = function(id){
		this.item = document.createElement("input");
        this.item.setAttribute("type", "text");
        this.item.setAttribute("id", id);
        this.addToDocument();
    }

}
TextField();
TextField.prototype = new Item();

function TextInput() {
  this.createTextInput = function(id, value, name) {
    this.item = document.createElement("INPUT");
    this.item.setAttribute("name", name);
    this.item.setAttribute("type", "text");
    this.item.setAttribute("id", id);
    this.item.setAttribute("value", value);
  }

  this.returnValue = function() {
    return this.item.value;
  }

  this.returnName = function() {
    return this.item.name;
  }
}
TextInput();
TextInput.prototype = new Item();

function SliderInput() {
  this.createSliderInput = function(id, c, min, max, value) {
    this.item = document.createElement("INPUT");
    this.item.setAttribute("type", "range");
    this.item.setAttribute("min", min);
    this.item.setAttribute("max", max);
    this.item.setAttribute("value", value);
    this.item.setAttribute("class", c);
    this.item.setAttribute("id", id);
  }

  this.returnValue = function() {
    return this.item.value;
  }

  this.returnName = function() {
    return this.item.name;
  }

  this.oninput = function() {
    console.log(document.getElementById("slider").value);
    return document.getElementById("slider").value;
  }
}
SliderInput();
SliderInput.prototype = new Item();
