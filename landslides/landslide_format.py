# This is the helper function that cleans up the raw data and transform into something we can use for the database

source_file = 'landslide_data_raw.txt'
target_file = 'landslide_formatted.csv'

f = open(source_file, encoding="ISO-8859-1")
t = open(target_file, 'w')

useful = [0, 1, 16, -3, -2]

states = {
    'Alabama': 'AL',
    'Alaska': 'AK',
    'Arizona': 'AZ',
    'Arkansas': 'AR',
    'California': 'CA',
    'Colorado': 'CO',
    'Connecticut': 'CT',
    'Delaware': 'DE',
    'District of Columbia': 'DC',
    'Florida': 'FL',
    'Georgia': 'GA',
    'Hawaii': 'HI',
    'Idaho': 'ID',
    'Illinois': 'IL',
    'Indiana': 'IN',
    'Iowa': 'IA',
    'Kansas': 'KS',
    'Kentucky': 'KY',
    'Louisiana': 'LA',
    'Maine': 'ME',
    'Maryland': 'MD',
    'Massachusetts': 'MA',
    'Michigan': 'MI',
    'Minnesota': 'MN',
    'Mississippi': 'MS',
    'Missouri': 'MO',
    'Montana': 'MT',
    'Nebraska': 'NE',
    'Nevada': 'NV',
    'New Hampshire': 'NH',
    'New Jersey': 'NJ',
    'New Mexico': 'NM',
    'New York': 'NY',
    'North Carolina': 'NC',
    'North Dakota': 'ND',
    'Northern Mariana Islands':'MP',
    'Ohio': 'OH',
    'Oklahoma': 'OK',
    'Oregon': 'OR',
    'Palau': 'PW',
    'Pennsylvania': 'PA',
    'Puerto Rico': 'PR',
    'Rhode Island': 'RI',
    'South Carolina': 'SC',
    'South Dakota': 'SD',
    'Tennessee': 'TN',
    'Texas': 'TX',
    'Utah': 'UT',
    'Vermont': 'VT',
    'Virgin Islands': 'VI',
    'Virginia': 'VA',
    'Washington': 'WA',
    'West Virginia': 'WV',
    'Wisconsin': 'WI',
    'Wyoming': 'WY',
}

for index, line in enumerate(f):
    line = line.strip()
    line = line.split('\t')

    if len(line) != 35 or line[3] != 'United States' or line[16] not in ['Small', 'Medium', 'Large']:
        continue

    info = ','.join(data for data in list(line[a] for a in useful))

    addr = line[4][1:-1].split(",")[-1].strip()
    if len(addr) == 2:
        info += ',' + addr.upper()

    elif addr in states:
        abbrev = states[addr]
        info += ',' + abbrev.upper()


    t.write(info + '\n')
